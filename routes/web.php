<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('football','logincontroller@getLogin');
Route::post('football','logincontroller@postLogin');
Route::group(['middleware' => 'checkrole'], function () {
    Route::resource('home','Admincontroller');
    Route::post('home/search','Admincontroller@searchTeam');
    Route::resource('team','TeamController');
    Route::get('logout','Admincontroller@getLogout');
});



    

