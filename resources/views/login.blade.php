<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Login</title>
  <!-- Custom fonts for this template-->
  <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
          <div class="alert alert-danger error errorLogin" style="display: none;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p style="color:red; display:none;" class="error errorLogin"></p>
          </div>
      <div class="card-body">
        <form action="#" method="POST" role="form" >
         @csrf
          <div class="form-group">
          <label for="inputEmail">Email address</label>
            <div class="form-label-group">
              <input type="email" id="inputEmail"  name="email" class="form-control" placeholder="Email address" required="required" autofocus="autofocus">
              <p style="color:red; display:none" class="error errorEmail"></p>
            </div>
          <div class="form-group">
          <label for="inputPassword">Password</label> 
            <div class="form-label-group">
              <input type="password" id="inputPassword"  name="password" class="form-control" placeholder="Password" required="required">
              <p style="color:red; display: none" class="error errorPassword"></p>
          </div>
          <div class="form-group">
            <div class="checkbox">
              <label>
                <input type="checkbox" value="remember-me">
                Remember Password
              </label>
            </div>
          </div>
             <button id="dang-nhap" type="submit" class="btn btn-primary btn-block">Login</button>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="register.html">Register an Account</a>
          <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- Core plugin JavaScript-->
  <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
</body>
<script src="{{ asset('js/ajaxLogin.js') }}"></script>
</html>
