<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('boostrap.css/bootstrap.min.css') }}">
</head>
<body>
<div class="container">

    <div class="row">
    @foreach($showTeams as $showTeam)
    <div class="col-md-3">
        <div class="card" style="width: 18rem;">
                <img class="img-thumbnail" src="{{asset('images')}}/{{$showTeam->image}}" alt="Card image cap" style="height:300px">
                <div class="card-body">
                    <h4>Club:{{$showTeam->team->team_name}}</h4>
                    <h5 class="card-title"><b>Name:</b>{{$showTeam->name}}</h5>
                    <p class="card-text"><b>Height:</b>{{$showTeam->height}}m-<b>Weight:</b>{{$showTeam->weight}}kg</p>
                    <p class="card-text"><b>National</b>:{{$showTeam->national}}</p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><b>Position:</b>{{$showTeam->position}}</li>
                    <li class="list-group-item"><b>Clothers_number:</b>{{$showTeam->clothers_number}}</li>
                </ul>
                </div>
    </div>
    @endforeach
    </div>
   
</div>
</body>
<script type="text/javascript" src="{{ asset('boostrap.js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('jquery/jquery-3.4.1.min.js') }}"></script>
</html>