<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <form method="POST" id="infoteam_form" class="form-horizontal" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">INFOMATION TEAM</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group" hidden="true">
                        <label class="control-label col-md-4">Title : </label>
                        <div class="col-md-8">
                            <input type="text" name="idinfo" id="idinfo" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Name </label>
                        <div class="col-md-8">
                            <input type="text" name="nameplayer" id="title" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">National </label>
                        <div class="col-md-8">
                            <input type="text" name="national" id="title" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Height </label>
                        <div class="col-md-8">
                            <input type="text" name="height" id="height" class="form-control" />
                            <p style="color:red; display: none" class="errorinfo errorheight"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Weight </label>
                        <div class="col-md-8">
                            <input type="text" name="weight" id="weight" class="form-control" />
                            <p style="color:red; display: none" class="errorinfo errorweight"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Position </label>
                        <div class="col-md-8">
                            <input type="text" name="position" id="position" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Clothers_number </label>
                        <div class="col-md-8">
                            <input type="text" name="clothers_number" id="clothers_number" class="form-control" />
                            <p style="color:red; display: none" class="errorinfo errorclothers"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Avata : </label>
                        <div class="col-md-8">
                            <input type="file" accept="image/*" onchange="loadFile(event)" name="image" id="image">
                            <img id="output" style="width:100px;height:100px" />
                            <p style="color:red; display: none" class="errorinfo errorimage"></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    var loadFile = function (event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
</script>