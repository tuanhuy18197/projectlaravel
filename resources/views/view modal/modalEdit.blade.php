<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="modal fade" id="editModal" aria-hidden="true">
<form data-url="" id="updateTeam" method="POST">
        @csrf
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Team</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group" hidden="true">
                <label for="Club" class="col-sm-2 control-label">Club</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="hidden_id" name="hidden_id"  placeholder="" value="" maxlength="50" >
                    <p style="color:red; display: none" class="error teamName"></p>
                </div>
            </div>
            <div class="form-group">
                <label for="Club" class="col-sm-2 control-label">Club</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="ajaxteamname" name="teamname" placeholder="" value="" maxlength="50" >
                    <p style="color:red; display: none" class="errorTeamname teamName errorhidden"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Master</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="ajaxmastername" name="mastername" placeholder="" value="" >
                    <p style="color:red; display: none" class="errorMasterName masterName errorhidden"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Number</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="ajaxpersonnumber" name="personnumber" placeholder="" value="" >
                    <p style="color:red; display: none" class="errorPersonNumber personNumber errorhidden"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Staium</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="ajaxnamestadium" name="namestadium" placeholder="" value="" >
                    <p style="color:red; display: none" class="errorNameStadium nameStadium errorhidden"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-6 control-label">Fan Number</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="ajaxfannumber" name="fannumber" placeholder="" value="" >
                    <p style="color:red; display: none" class="errorFanNumber fanNumber errorhidden"></p>
                </div>
            </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn-save">Save changes</button>
      </div>
    </div>
  </div>
</form>
</div>
</body>
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/ajaxUpdateTeam.js') }}"></script>
</html>



