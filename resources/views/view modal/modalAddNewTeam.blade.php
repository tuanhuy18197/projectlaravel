<div class="modal fade" id="exampleModal" aria-hidden="true">
<form data-url="" id="addnew" method="POST">
        @csrf
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Team</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label for="Club" class="col-sm-2 control-label">Club</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="team_name" name="team_name" placeholder="" value="" maxlength="50" >
                    <p style="color:red; display: none" class="error errorteamName"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Master</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="master_name" name="master_name" placeholder="" value="" >
                    <p style="color:red; display: none" class="error errormasterName"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Number</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="person_number" name="person_number" placeholder="" value="" >
                    <p style="color:red; display: none" class="error errortpersonNumber"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Staium</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name_stadium" name="name_stadium" placeholder="" value="" >
                    <p style="color:red; display: none" class="error errornameStadium"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-6 control-label">Fan Number</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="fan_number" name="fan_number" placeholder="" value="" >
                    <p style="color:red; display: none" class="error errorfanNumber"></p>
                </div>
            </div>     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="create">Save changes</button>
      </div>
    </div>
  </div>
</form>
</div>
</body>
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</html>



