    @foreach($teams as $team)
    <tr>
        <td>{{$team->team_name}}</td>
        <td>{{$team->master_name}}</td>
        <td>{{$team->person_number}}</td>
        <td>{{$team->name_stadium}}</td>
        <td>{{$team->fan_number}}</td>
        <td>
            <button type="button" class="btn btn-success editTeam" id="editTeam" data-id="{{$team->id}}"
                value="{{$team->id}}" data-toggle="modal" data-target="#editModal">
                <i class="fas fa-edit"></i>
            </button>
            <button type="button" class="btn btn-danger deleteTeam" id="deleteTeam" data-id="{{$team->id}}"
                value="{{$team->id}}" data-toggle="modal" data-target="#deleteModal">
                <i class="fas fa-trash"></i>
            </button>
            <button type="button" class="btn btn-warning infoTeam" id="infoTeam" data-id="{{$team->id}}"
                value="{{$team->id}}" data-toggle="modal" data-target="#infoModal">
                <i class="fas fa-plus-square"></i>
            </button>
            <a href="/team/info/{{$team->id}}" type="button" class="btn btn-info showInfoTeam" id="showInfoTeam"
                data-id="{{$team->id}}" value="{{$team->id}}" data-target="#showInfoTeam">
                <i class="fas fa-eye"></i>
            </a>
        </td>
    </tr>
    @endforeach
    <script src="{{ asset('js/ajaxAddNewTeam.js') }}"></script>
    <script src="{{ asset('js/ajaxEditTeam.js') }}"></script>
    <script src="{{ asset('js/ajaxUpdateTeam.js') }}"></script>
    <script src="{{ asset('js/ajaxDeleteTeam.js') }}"></script>
    <script src="{{ asset('js/ajaxDeleteTeam.js') }}"></script>
    <script src="{{ asset('js/ajaxInfoTeam/ajaxCreateInfo.js') }}"></script>