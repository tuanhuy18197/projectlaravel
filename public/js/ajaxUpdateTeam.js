$(document).ready(function () {
    $('#updateTeam').submit(function(e) {
        e.preventDefault();
        var id = $('#hidden_id').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'PUT',
            url: '/home/' + $("#hidden_id").val(),
            data: {
                'id': $('#hidden_id').val(),
                'team_name' : $('#ajaxteamname').val(),
                'master_name' : $('#ajaxmastername').val(),
                'person_number': $('#ajaxpersonnumber').val(),
                'name_stadium': $('#ajaxnamestadium').val(),
                'fan_number': $('#ajaxfannumber').val()
            },
            dataType: 'json',
            success: function (data) {
                $('#editModal').modal('hide');
                window.location.reload(); 
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $('.errorhidden').hide();
                if (errors.errors.team_name != undefined) {
                  $('.errorTeamname').show().text(errors.errors.team_name[0]);
                }
                if (errors.errors.master_name != undefined) {
                    $('.errorMasterName').show().text(errors.errors.master_name[0]);
                }
                if (errors.errors.person_number != undefined) {
                    $('.errorPersonNumber').show().text(errors.errors.person_number[0]);
                }
                if (errors.errors.name_stadium != undefined) {
                    $('.errorNameStadium').show().text(errors.errors.name_stadium[0]);
                }
                if (errors.errors.fan_number != undefined) {
                    $('.errorFanNumber').show().text(errors.errors.fan_number[0]);
                }
            },
        });
    });
});