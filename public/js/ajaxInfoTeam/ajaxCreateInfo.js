$(document).ready(function () {
    $('.infoTeam').click(function (e) { 
        e.preventDefault();
        var id=$(this).val();
        $('#idinfo').val(id); 
    });
    $('#infoteam_form').submit(function(e){
        e.preventDefault();
        var id=$('#infoTeam').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
             url:'/team/',
             data:new FormData(this),
             method:'POST',
             contentType: false,
             cache: false,
             processData: false,
             dataType: 'json',
             success:function(data)
             {
                 console.log(data);
                 $('#infoModal').modal('hide');
                 $('.modal-backdrop').removeClass('show');
                 location.href = location.href;
                 window.location.reload(); 
             },
             error:function(data)
             {
                var errors = $.parseJSON(data.responseText);
                console.log(errors);
                $('.errorinfo').hide();
                if (errors.errors.height != undefined) {
                   $('.errorheight').show().text(errors.errors.height);
                }
                if (errors.errors.weight != undefined) {
                    $('.errorweight').show().text(errors.errors.weight);
                }
                if (errors.errors.clothers_number != undefined) {
                    $('.errorclothers').show().text(errors.errors.clothers_number);
                }
                if (errors.errors.image!= undefined) {
                    $('.errorimage').show().text(errors.errors.image);
                }
             },
        });
    });
});