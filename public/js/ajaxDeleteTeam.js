$(document).ready(function () {
    $('.delete').click(function (e) { 
        e.preventDefault();
        var id=$('#deleteTeam').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
             type: 'delete',
             url: '/home/' + $('#deleteTeam').val(),
             success: function (data) {
                 console.log(data);
                 console.log(data.success);
                 $('#deleteModal').modal('hide');
                 window.location.reload();
             }
        });
    });
});