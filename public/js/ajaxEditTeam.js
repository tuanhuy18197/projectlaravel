$(document).ready(function () {
    $('.editTeam').click(function(e) {
        e.preventDefault();
        var id = $(this).val();
        console.log(id);
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
               url: "/home/"+id+"/edit",
               dataType:"json",
               success:function(html){
                    $('#hidden_id').val(html.data.id);
                    $('#ajaxteamname').val(html.data.team_name);
                    $('#ajaxmastername').val(html.data.master_name);
                    $('#ajaxpersonnumber').val(html.data.person_number);
                    $('#ajaxnamestadium').val(html.data.name_stadium);
                    $('#ajaxfannumber').val(html.data.fan_number);
               }     
        });
    });
});      