$(document).ready(function () {
    $('#addnew').submit(function(e) {
        e.preventDefault();
        var url= $(this).attr('data-url');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            'url': "/home/" ,
            'data': {
                    'team_name' : $('#team_name').val(),
                    'master_name' : $('#master_name').val(),
                    'person_number': $('#person_number').val(),
                    'name_stadium': $('#name_stadium').val(),
                    'fan_number': $('#fan_number').val()
            },
            'type': 'POST',
             dataType: 'json',
             success: function (data) {
                    console.log(data);
                    location.href = location.href;
                    $('#exampleModal').modal('hide');
                    $('.modal-backdrop').removeClass('show');
                    window.location.reload();  
            },
             error: function (data) {
                 console.log(data);
                    var errors = $.parseJSON(data.responseText);
                    console.log(errors.errors.team_name);
                    $('.error').hide();
                    if (errors.errors.team_name != undefined) {
                        $('.errorteamName').show().text(errors.errors.team_name);
                    }
                    if (errors.errors.master_name != undefined) {
                        $('.errormasterName').show().text(errors.errors.master_name);
                    }
                    if (errors.errors.person_number != undefined) {
                        $('.errortpersonNumber').show().text(errors.errors.person_number);
                    }
                    if (errors.errors.name_stadium != undefined) {
                        $('.errornameStadium').show().text(errors.errors.name_stadium);
                    }
                    if (errors.errors.fan_number != undefined) {
                        $('.errorfanNumber').show().text(errors.errors.fan_number);
                    }  
            }
        });
    });
});