$(document).ready(function () {
    $('#country_name').on('keyup',function(e) {
        e.preventDefault();
        var value=$(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
              type:'post',
              url: '/home/search/',
              data:{
                   'search': value,
              },
              success:function(data){
                  console.log(data.team);
                  console.log(data.html);
                  $('#table-body').html(data.html);
              }    
        });
    });
});
