<?php
namespace App;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\FormDataRequest;
use App\Http\Requests\updateRequest;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Database\Eloquent\Collection;

class Team extends Model
{
    protected $table='teams';
    protected $fillable = [
       'id','user_id','team_name', 'master_name', 'person_number','name_stadium','fan_number',
    ];
    public $timestamps = false;
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function infoteam()
    {
        return $this->hasMany('App\InfoTeam');
    }
    public static function teamIndex()
    {
        $getTeams = Team::orderBy('id', 'desc')->paginate(3);
        return $getTeams;
    }
    public static function teamStore(FormDataRequest $request)
    {
        $userID = Auth::id();
        $team = new Team();
        $team->user_id = $userID;
        $team->team_name = $request->team_name;
        $team->master_name = $request->master_name;
        $team->person_number = $request->person_number;
        $team->name_stadium = $request->name_stadium;
        $team->fan_number = $request->fan_number;
        $team->save();
        return $team;
    }
    public static function teamEdit($id)
    {
        if(request()->ajax())
        {
            $data = Team::findOrFail($id);
            return $data;
        }
    }
    public static function teamUpdate(updateRequest $request,$id)
    {
        $team = Team::findOrFail($id);
        $team->team_name = $request->team_name;
        $team->master_name = $request->master_name;
        $team->person_number = $request->person_number;
        $team->name_stadium = $request->name_stadium;
        $team->fan_number = $request->fan_number;
        $team->save();
        return $team;
    }
    public static function teamDetroy($id)
    {
        $team = Team::findOrFail($id);
        $team->delete();
        return $team;
    }
    public static function searchTeam(Request $request){
        $id = Auth::id();
        if ($request->ajax()) {
        $team = Team::where('user_id',$id)
                    ->where('team_name','LIKE','%'.$request->search.'%')->get();
        return $team;
        }
    }
}
