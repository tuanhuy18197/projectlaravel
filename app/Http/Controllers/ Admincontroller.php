<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use App\User;
use App\Team;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Requests\FormDataRequest;
use App\Http\Requests\updateRequest;


class Admincontroller extends Controller
{
    public function index()
    {
        $getTeams=Team::teamIndex();
        return view('home', ['getTeams' => $getTeams]);
    }
    public function store(FormDataRequest $request)
    { 
        $team = Team::teamStore($request); 
        return response()->json(['data'=>$team]);   
    }
    public function edit($id)
    {
        $editTeam = Team::teamEdit($id);
        return response()->json(['data' => $editTeam]);
    }
    public function update(updateRequest $request,$id)
    {
        $updateTeam = Team::teamUpdate($request,$id);
        return response()->json(['data'=>$updateTeam]);
    }
    public function destroy($id)
    {
        $detroyTeam = Team::teamDetroy($id);
        return response()->json(['data'=>$detroyTeam]);
    }
    public function getLogout() {
        Auth::logout();
        return redirect('football');
    }
    public function searchTeam(Request $request)
    {
        $team = Team::searchTeam($request);
        $html = view('tableAdmin', ['teams' => $team])->render();
        return response()->json([
            'team' => $team,
            'html' => $html,
        ]);
    }
    public function create()
    {  
       //
    }
}


