<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Auth;
use App\Infoteam;
use Illuminate\Support\MessageBag;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Requests\CreateInfoTeam;

class TeamController extends Controller
{
    public function index()
    {   $showTeam = Infoteam::showTeam();
        return view('viewInfoTeam.showInfo',['showTeams' => $showTeam]);
    }
    public function create()
    {
        //
    }
    public function show($id){
        $showTeam = Infoteam::showTeam($id);
        return view('viewInfoTeam.showInfo',['showTeams' => $showTeam]);
    }
    public function store(CreateInfoTeam $request)
    {
        $infoTeam = Infoteam::infoTeamStore($request);
        return response()->json(['data'=> $infoTeam,'success'=>'thành công']); 
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request,$id)
    {
      
    }
    public function destroy($id)
    {
        //
    }
}
