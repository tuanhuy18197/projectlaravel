<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class CreateInfoTeam extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'height'   =>'required|integer|',
            'weight' =>'required|integer|',
            'clothers_number' =>'required|integer',
            'image'   => 'required|image|max:2048',
          
        ];
    }
}
