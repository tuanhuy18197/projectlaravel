<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\UppercaseRule;
class FormDataRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'team_name' => [
                'required',
                new UppercaseRule(),
            ],
            'master_name' => 'required|',
            'person_number' =>'required|integer|',
            'name_stadium' =>'required|',
            'fan_number' =>'required|integer|',
        ];
    }
    public function messages()
    {
        return [
            

        ];
    }
}
