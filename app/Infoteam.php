<?php
namespace App;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Http\Requests\CreateInfoTeam;

class Infoteam extends Model
{
    protected $table= 'info_teams';
    protected $fillable=['id','team_id','name','national','height','weight','position','clothers_number','image',];
    public $timestamps = false;
    public function team()
    {
        return $this->belongsTo('App\Team');
    }
    public static function infoTeamStore(CreateInfoTeam $request){
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath, $name);
                $infoTeam = new Infoteam();
                $infoTeam->team_id = $request->idinfo;
                $infoTeam->name = $request->nameplayer;
                $infoTeam->national = $request->national;
                $infoTeam->height = $request->height;
                $infoTeam->weight = $request->weight;
                $infoTeam->position = $request->position;
                $infoTeam->clothers_number = $request->clothers_number;
                $infoTeam->image = $name;
                $infoTeam->save();
                return $infoTeam; 
            }
    }
    public static function showTeam($id){
             $showTeam = Infoteam::where('team_id',$id)->get();
             return $showTeam;
    }
}
