<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('info_teams', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('team_id')->unsigned()->index();
                $table->string('title');
                $table->string('article');
                $table->string('image');
                $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
        
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_teams');
    }
}
